@extends('admin._layouts.main')

@section('historiActive')
    {{ 'active' }}
@endsection

@section('content-admin')
    <div class="main-wrapper">
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        Histori 
                    </h2>
                </div>
            </div>
        </div>
        <div class="bg-white flex-wrap flex-md-nowrap align-items-center pt-3 pb-4 pl-4 pr-4 mb-3 border-bottom">
            <div class="col-sm-12 d-flex justify-content-center mt-4">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Waktu Buat</th>
                            <th scope="col">Keterangan</th>
                            <th scope="col">ID Konten</th>
                            <th scope="col">ID Transaksi</th>
                            <th scope="col">Kode</th>
                            <th scope="col">Nama Transaksi</th>
                            <th scope="col">Debit</th>
                            <th scope="col">Kredit</th>
                            <th scope="col">Saldo</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0 ?>
                        @foreach($histori as $item)
                        <?php $i++ ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
@endsection