@extends('admin._layouts.main')

@section('jawaban_personalActive')
    {{ 'active' }}
@endsection

@section('content-admin')
    <div class="main-wrapper">
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-auto">
                    <h2 class="page-title">
                        Jawaban Dari Personal
                    </h2>
                </div>
            </div>
        </div>
        <div class="bg-white flex-wrap flex-md-nowrap align-items-center pt-3 pb-4 pl-4 pr-4 mb-3 border-bottom">                 
            <div class="col-sm-12 d-flex justify-content-center mt-4">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Waktu Buat</th>
                        <th scope="col">Kategori</th>
                        <th scope="col">Jenjang</th>
                        <th scope="col">ID Pertanyaan</th>
                        <th scope="col">Pertanyaan</th>
                        <th scope="col">Username</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Scoin</th>
                        <th scope="col">Scoin UP</th>
                        <th scope="col">Approve</th>
                        <th scope="col">Poin</th>
                        <th scope="col">Scoin</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0 ?>
                        @foreach($pitakon as $item)
                        <?php $i++ ?>
                        <tr>
                            <td>{{ $i }}</td>
                            <td>{{$item->created_at}}</td>
                            @foreach ($item->category as $data)
                                <td>{{ $data->category }}</td>
                                <td>{{ $data->ladder }}</td>
                            @endforeach
                            <td>{{ $item->id }}</td>
                            <td>{{$item->essay}}</td>
                            @foreach($item->user as $data)
                                <td>{{ $data->username }}</td>
                                <td>{{ $data->name }}</td>
                            @endforeach
                            <td>-</td>
                            <td>-</td>
                            <td>
                                <div class="form-check form-switch">
                                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                                </div>
                            </td>
                            <td>-</td>
                            <td>-</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>  
        </div>
    </div>
@endsection
