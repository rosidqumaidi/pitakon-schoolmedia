<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">

    <title>Pitakon A2</title>
</head>
<body>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg" style="background: -webkit-linear-gradient(0deg, #98b1c3 19.5%, #014282 19.5%);">
        <div class="col-sm-2" style="width: 20%!important;">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">
                    <img src="https://schoolmedia.id/assets/img/site-logo.png" style="width: 100%;">
                </a>
            </div>
        </div>
        <div class="col-sm-9" style="width: 80%!important;">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <h5>Pitakon</h5>
                        </li>
                    </ul>
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                          <a class="nav-link text-white pr-3 pl-3" href="#">
                              <img src="assets/icon/notif.svg" width="20px" height="20px">
                          </a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link text-white pr-3 pl-3" href="#">
                                <img src="assets/icon/inbox.svg" width="20px" height="20px">
                            </a>
                          </li>
                        <li class="nav-item">
                          <a class="nav-link text-white pr-3 pl-3" href="pitakona1">
                              <img src="assets/icon/home.svg" width="20px" height="20px">
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link text-white pr-3 pl-3" href="pitakon1" style="margin-top: 2px;"><b>Peter</b></a>
                        </li>
                   </ul>
                </div>
            </div>
        </div>
    </nav>

    <!-- Content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-lg-3" style="width: 20%!important; padding-right: 0px; padding-left: 0px;">
                
                <nav id="sidebarMenu" class="d-md-block bg-white sidebar collapse">
                    <div class="container">
                        <div class="position-sticky pt-4">
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="assets/img/bdd.png" class="rounded-circle" width="65px" height="65px;">
                                </div>
                                <div class="col-sm-8 mt-2">
                                    <a class="text-schoolmedia" style="text-decoration: none; font-size: 18px;" href="#">
                                        <b>Petter Watson</b>
                                    </a>
                                    <p style="font-size: 13px; font-weight: bold; color: #9d9d9d;">
                                        SMAN 1 Klaten
                                    </p>
                                </div>
                            </div>
    
                            
                            <div class="row">
                                <div class="div mt-5">
                                    <ul class="nav nav-pills" id="pills-tab" role="tablist" style="font-size: 15px; font-weight: bold;">
                                        <li class="nav-item col-4" role="presentation">
                                          <h6 class="mt-1" style="font-size: 15px;">Kategori</h6>
                                        </li>
                                        <li class="nav-item col-5" role="presentation">
                                          <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="text-align: center; padding-top: 5px; padding-bottom: 5px;">Kurikulum</a>
                                        </li>
                                        <li class="nav-item col-3" role="presentation">
                                          <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="text-align: center; padding-top: 5px; padding-bottom: 5px;">Tema</a>
                                        </li>
                                    </ul>     
                                </div>
                                  <div class="tab-content mb-3  " id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <hr>
    
                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/mtk.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>Matematika</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
    
                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/ppkn.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>PPKN</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/kimia.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>Kimia</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/indo.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>Bahasa Indonesia</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/seni.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>Seni Rupa</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/olahraga.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>Olahraga</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
    
                                    </div>
                                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        <hr>
    
                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/mtk.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>Matematika</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
    
                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/ppkn.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>PPKN</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/kimia.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>Kimia</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/indo.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>Bahasa Indonesia</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/seni.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>Seni Rupa</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>

                                        <div class="row">
                                            <a href="#" style="text-decoration: none; color: #000;">
                                                <div class="row">
                                                    <div class="col-sm-1 mt-1">
                                                        <img src="assets/icon/olahraga.svg" style="width: 20px; height: 20px;">
                                                    </div>
                                                    <div class="col-sm-10 mt-2 ml-2">
                                                        <h6>Olahraga</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
    
                                    </div>
                                    
                                  </div>
                            </div>
                    
                        </div>
                    </div>
                </nav>


                <nav id="sidebarMenu" class="mt-2 d-md-block bg-white sidebar collapse">
                    <div class="container">
                        <div class="position-sticky pt-2 pb-2">
                            <div class="row justify-content-center">
                                <div class="col-sm-12 mt-3">
                                    <h6>RANKING BAHASA INDONESIA</h6>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-sm-8 mt-3">
                                    <h6>Muryanto</h6>
                                </div>
                                <div class="col-sm-4 mt-3 d-flex justify-content-end">
                                    <h6>50</h6>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-sm-6 mt-3">
                                    <h6>Auriley</h6>
                                </div>
                                <div class="col-sm-6 mt-3 d-flex justify-content-end">
                                    <h6>25</h6>
                                </div>
                            </div>
    
                            <div class="row">
                                <div class="col-sm-6 mt-3">
                                    <h6>Masdim09</h6>
                                </div>
                                <div class="col-sm-6 mt-3 d-flex justify-content-end">
                                    <h6>15</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>

            </div>
            

            
        
            <main class="col-md-6 col-lg-6 px-md-4" style="width: 60%!important;">
                <div class="bg-white flex-wrap flex-md-nowrap align-items-center pt-3 pb-4 pl-4 pr-4 mb-3 border-bottom">
                    
                    <div class="row">
                        <div class="col-sm-12 d-flex justify-content-end mt-2">
                            <img src="assets/img/bdd.png" style="width: 100%; height: 200px; border-radius: 8px;">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 d-flex justify-content-end mt-4">
                            <a href="#" class="btn btn-md bg-schoolmedia text-white" style="border-radius: 0; padding-right: 25px; padding-left: 25px;">Bertanya</a>
                        </div>
                    </div>
                    
                </div>


                <div class="bg-white flex-wrap flex-md-nowrap align-items-center pt-3 pb-4 pl-4 pr-4 mb-3 border-bottom">
                    
                    <div class="col-sm-12 d-flex justify-content-center mt-4">
                        <div class="form border-sch" style="padding: 0.8px;">
                            <div class="post-textarea">
                                <textarea name="" id="" cols="85" rows="5" placeholder="Ketik Pertanyaanmu Disini..." style="border: 0; padding: 20px; border-radius: 6px;"></textarea>
                            </div>
                            <div class="post-footer rounded-bottom" style="padding: 8px; padding-right: 10px; padding-left: 20px; background-color: #dee2e6;">
                                <div class="row">
                                    <div class="col-sm-4 mt-2">
                                        <b style="color: #014282;">Tambahkan File</b>
                                    </div>
                                    <div class="col-sm-4 mt-2">
                                        <b style="color: #014282;">Kepada: <span class="ml-2" style="background-color: #b7b7b7; padding: 4px; border-radius: 6px;">Tri Aditya</span></b> 
                                    </div>
                                    <div class="col-sm-4">
                                        <a href="#" class="btn btn-md bg-schoolmedia text-white" style="border-radius: 0; float: right; padding-right: 25px; padding-left: 25px;">
                                            <img class="mr-2" src="assets/icon/upload.svg" width="25px" height="25px">
                                            Unggah
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        
                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Kategori</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>Kurikulum/Tema</option>
                                    <option value="1">14 Hari</option>
                                    <option value="2">1 Bulan</option>
                                    <option value="3">1 Tahun</option>
                                </select>
                            </div>
    
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Jenis Kelompok Mapel</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>SMA/SMK/MA</option>
                                    <option value="1">SMP/MTS</option>
                                    <option value="2">SD/MI</option>
                                </select>
                            </div>
    
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Nilai Pertanyaan</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>Semua</option>
                                    <option value="1">SMP/MTS</option>
                                    <option value="2">SD/MI</option>
                                </select>
                            </div>
    
                        </div>

                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Jenjang</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>Kurikulum/Tema</option>
                                    <option value="1">14 Hari</option>
                                    <option value="2">1 Bulan</option>
                                    <option value="3">1 Tahun</option>
                                </select>
                            </div>
    
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Mata Pelajaran</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>SMA/SMK/MA</option>
                                    <option value="1">SMP/MTS</option>
                                    <option value="2">SD/MI</option>
                                </select>
                            </div>
    
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Poin</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>Semua</option>
                                    <option value="1">SMP/MTS</option>
                                    <option value="2">SD/MI</option>
                                </select>
                            </div>
                        </div>

                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Kelompok Mata Pelajaran</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>Kurikulum/Tema</option>
                                    <option value="1">14 Hari</option>
                                    <option value="2">1 Bulan</option>
                                    <option value="3">1 Tahun</option>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Kelas</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>SMA/SMK/MA</option>
                                    <option value="1">SMP/MTS</option>
                                    <option value="2">SD/MI</option>
                                </select>
                            </div>

                            <div class="col-sm-3"></div>

                            <div class="col-sm-3">
                                <a href="#" class="btn bg-schoolmedia text-white mt-4" style="width: 100%;">
                                    <img class="mr-2" src="assets/icon/upload.svg" width="25px" height="25px">
                                    Kirim Pertanyaan
                                </a>
                            </div>
                        </div>

                    </div>
                    
                </div>

                <div class="bg-white flex-wrap flex-md-nowrap align-items-center pt-3 pb-4 pl-4 pr-4 mb-3 border-bottom">
                    

                    <div class="col-sm-12">
                        
                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Jenis Kelompok Mapel</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>Kurikulum/Tema</option>
                                    <option value="1">14 Hari</option>
                                    <option value="2">1 Bulan</option>
                                    <option value="3">1 Tahun</option>
                                </select>
                            </div>
    
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Nilai Pertanyaan</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>SMA/SMK/MA</option>
                                    <option value="1">SMP/MTS</option>
                                    <option value="2">SD/MI</option>
                                </select>
                            </div>
    
                            <div class="col-sm-3"></div>
    
                        </div>

                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Mata Pelajaran</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>Semua</option>
                                    <option value="1">SMP/MTS</option>
                                    <option value="2">SD/MI</option>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Poin</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>Kurikulum/Tema</option>
                                    <option value="1">14 Hari</option>
                                    <option value="2">1 Bulan</option>
                                    <option value="3">1 Tahun</option>
                                </select>
                            </div>
    
                            <div class="col-sm-3"></div>

                        </div>

                        <div class="row mt-4">
                            <div class="col-sm-3">
                                <label for="form-select" style="font-size: 15px; color: #585555;"><b>Kelas</b></label>
                                <select class="form-select mt-2" aria-label="Default select example">
                                    <option selected>Kurikulum/Tema</option>
                                    <option value="1">14 Hari</option>
                                    <option value="2">1 Bulan</option>
                                    <option value="3">1 Tahun</option>
                                </select>
                            </div>

                            <div class="col-sm-3"></div>

                            <div class="col-sm-3"></div>

                            <div class="col-sm-3">
                                <a href="#" class="btn bg-schoolmedia text-white mt-4" style="width: 100%;">
                                    <img class="mr-2" src="assets/icon/upload.svg" width="25px" height="25px">
                                    Kirim Pertanyaan
                                </a>
                            </div>
                        </div>

                    </div>
                    
                </div>


            </main>

            <div class="col-md-3 col-lg-3" style="width: 20%!important; padding-right: 0px; padding-left: 0px;">
                
                <nav id="sidebarMenu" class="d-md-block bg-white sidebar collapse">
                    <div class="container" style="padding-right: 2rem; padding-left: 2rem;">
                        <div class="position-sticky pt-4">
                            
                            <div class="row">
                                  
                                  <div class="tab-content mb-3  " id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                        <h6 class="text-schoolmedia">Tingkatkan Poin Dengan Membantu Menjawab Pertanyaan Berikut</h6>
                                        <hr>
    
                                        <div class="overflow-auto" style="height: 350px;">
                                            
                                            <div class="row" style="margin: 0;">
                                                <div class="col-sm-12 mt-2">
                                                    <h6>Jawablah 5 Pertanyaan Sejarah dalam 48 Jam</h6>
                                                </div>
                                                <div class="row" style="font-size: 10px; font-weight: bold;">
                                                    <div class="col-sm-3">
                                                        <p>0/5</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>2H</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>+5 Poin</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>Mulai</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="margin: 0;">
                                                <div class="col-sm-12 mt-2">
                                                    <h6>Jawablah 5 Pertanyaan Sejarah dalam 48 Jam</h6>
                                                </div>
                                                <div class="row" style="font-size: 10px; font-weight: bold;">
                                                    <div class="col-sm-3">
                                                        <p>0/5</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>2H</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>+5 Poin</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>Mulai</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="margin: 0;">
                                                <div class="col-sm-12 mt-2">
                                                    <h6>Jawablah 5 Pertanyaan Sejarah dalam 48 Jam</h6>
                                                </div>
                                                <div class="row" style="font-size: 10px; font-weight: bold;">
                                                    <div class="col-sm-3">
                                                        <p>0/5</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>2H</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>+5 Poin</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>Mulai</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="margin: 0;">
                                                <div class="col-sm-12 mt-2">
                                                    <h6>Jawablah 5 Pertanyaan Sejarah dalam 48 Jam</h6>
                                                </div>
                                                <div class="row" style="font-size: 10px; font-weight: bold;">
                                                    <div class="col-sm-3">
                                                        <p>0/5</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>2H</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>+5 Poin</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>Mulai</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row" style="margin: 0;">
                                                <div class="col-sm-12 mt-2">
                                                    <h6>Jawablah 5 Pertanyaan Sejarah dalam 48 Jam</h6>
                                                </div>
                                                <div class="row" style="font-size: 10px; font-weight: bold;">
                                                    <div class="col-sm-3">
                                                        <p>0/5</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>2H</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>+5 Poin</p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p>Mulai</p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
    
                                    </div>
    
                                  </div>
                            </div>
                    
                        </div>
                    </div>
                </nav>


                <nav id="sidebarMenu" class="mt-3 d-md-block bg-white sidebar collapse">
                    <div class="container" style="padding-right: 2rem; padding-left: 2rem;">
                        <div class="position-sticky pt-2 pb-2">
                            <div class="row justify-content-center">
                                <div class="col-sm-12 mt-3">
                                    <h6 class="text-schoolmedia">Daftar Pertanyaan-Ku</h6>
                                    <hr>
                                </div>
                            </div>
    
                            <div class="overflow-auto" style="height: 350px;">
                                            
                                <div class="row" style="margin: 0;">
                                    <div class="col-sm-12 mt-2">
                                        <h6>Jawablah 5 Pertanyaan Sejarah dalam 48 Jam</h6>
                                    </div>
                                    <div class="row" style="font-size: 10px; font-weight: bold;">
                                        <div class="col-sm-3">
                                            <p>0/5</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>2H</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>+5 Poin</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>Mulai</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin: 0;">
                                    <div class="col-sm-12 mt-2">
                                        <h6>Jawablah 5 Pertanyaan Sejarah dalam 48 Jam</h6>
                                    </div>
                                    <div class="row" style="font-size: 10px; font-weight: bold;">
                                        <div class="col-sm-3">
                                            <p>0/5</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>2H</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>+5 Poin</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>Mulai</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin: 0;">
                                    <div class="col-sm-12 mt-2">
                                        <h6>Jawablah 5 Pertanyaan Sejarah dalam 48 Jam</h6>
                                    </div>
                                    <div class="row" style="font-size: 10px; font-weight: bold;">
                                        <div class="col-sm-3">
                                            <p>0/5</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>2H</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>+5 Poin</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>Mulai</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin: 0;">
                                    <div class="col-sm-12 mt-2">
                                        <h6>Jawablah 5 Pertanyaan Sejarah dalam 48 Jam</h6>
                                    </div>
                                    <div class="row" style="font-size: 10px; font-weight: bold;">
                                        <div class="col-sm-3">
                                            <p>0/5</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>2H</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>+5 Poin</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>Mulai</p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin: 0;">
                                    <div class="col-sm-12 mt-2">
                                        <h6>Jawablah 5 Pertanyaan Sejarah dalam 48 Jam</h6>
                                    </div>
                                    <div class="row" style="font-size: 10px; font-weight: bold;">
                                        <div class="col-sm-3">
                                            <p>0/5</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>2H</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>+5 Poin</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <p>Mulai</p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div>
                </nav>

            </div>

        </div>

    </section>

    



    <!-- JS -->
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>