@extends('user._layouts.main')

@section('historiActive')
    {{ 'active' }}
@endsection

@section('content-user')
    <section class="content">
        <div class="row">
            <main class=" px-md-4" style=" padding-right: 0px!important; padding-left: 10px!important;">
                <div class="bg-white flex-wrap flex-md-nowrap align-items-center pt-3 pb-4 pl-4 pr-4 mb-3 border-bottom">
                    <div class="col-sm-12 d-flex justify-content-center mt-4">
                        <table class="table table-striped">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Waktu Buat</th>
                                <th scope="col">Keterangan</th>
                                <th scope="col">ID Konten</th>
                                <th scope="col">ID Transaksi</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Nama Transaksi</th>
                                <th scope="col">Debit</th>
                                <th scope="col">Kredit</th>
                                <th scope="col">Saldo</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th scope="row">1</th>
                                <td>01/05/2020</td>
                                <td>Dari akun ke akun</td>
                                <td>3/A/N/BHS/Ci/xxxx</td>
                                <td>5110-xxxxx</td>
                                <td>5110</td>
                                <td>Bertanya</td>
                                <td>1000</td>
                                <td>2000</td>
                                <td>5000</td>
                              </tr>
                              <tr>
                                <th scope="row">2</th>
                                <td>01/05/2020</td>
                                <td>Dari akun ke akun</td>
                                <td>3/A/N/BHS/Ci/xxxx</td>
                                <td>5110-xxxxx</td>
                                <td>5110</td>
                                <td>Bertanya</td>
                                <td>1000</td>
                                <td>2000</td>
                                <td>5000</td>
                              </tr>
                              <tr>
                                <th scope="row">3</th>
                                <td>01/05/2020</td>
                                <td>Dari akun ke akun</td>
                                <td>3/A/N/BHS/Ci/xxxx</td>
                                <td>5110-xxxxx</td>
                                <td>5110</td>
                                <td>Bertanya</td>
                                <td>1000</td>
                                <td>2000</td>
                                <td>5000</td>
                              </tr>
                              <tr>
                                <th scope="row">4</th>
                                <td>01/05/2020</td>
                                <td>Dari akun ke akun</td>
                                <td>3/A/N/BHS/Ci/xxxx</td>
                                <td>5110-xxxxx</td>
                                <td>5110</td>
                                <td>Bertanya</td>
                                <td>1000</td>
                                <td>2000</td>
                                <td>5000</td>
                              </tr>
                              <tr>
                                <th scope="row">5</th>
                                <td>01/05/2020</td>
                                <td>Dari akun ke akun</td>
                                <td>3/A/N/BHS/Ci/xxxx</td>
                                <td>5110-xxxxx</td>
                                <td>5110</td>
                                <td>Bertanya</td>
                                <td>1000</td>
                                <td>2000</td>
                                <td>5000</td>
                              </tr>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </main>
        </div>
    </section>
@endsection