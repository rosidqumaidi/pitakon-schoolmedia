

    <section class="content">
        <div class="row">
            <main class=" px-md-4" style="padding-right: 0px!important; padding-left: 10px!important;">
                <div class="bg-white flex-wrap flex-md-nowrap align-items-center pt-3 pb-4 pl-4 pr-4 mb-3 border-bottom">
                    <div class="col-sm-12 d-flex justify-content-center mt-4">
                        <table class="table table-striped">
                            <thead>
                              <tr>
                                <th scope="col">No</th>
                                <th scope="col">Waktu Buat</th>
                                <th scope="col">Kategori</th>
                                <th scope="col">Jenjang</th>
                                <th scope="col">Pertanyaan</th>
                                <th scope="col">Username</th>
                                <th scope="col">Nama</th>
                                <th scope="col">JU</th>
                                <th scope="col">Scoin</th>
                                <th scope="col">Poin</th>
                                <th scope="col">Menjawab</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <th scope="row">1</th>
                                <td>01/05/2020</td>
                                <td>Kurikulum</td>
                                <td>SMK</td>
                                <td>Detail</td>
                                <td>Kangadit@gmail.com</td>
                                <td>Adit</td>
                                <td>SI</td>
                                <td>2000</td>
                                <td>5</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-primary">Jawab</a>
                                </td>
                              </tr>
                              <tr>
                                <th scope="row">2</th>
                                <td>01/05/2020</td>
                                <td>Kurikulum</td>
                                <td>SMK</td>
                                <td>Detail</td>
                                <td>Kangadit@gmail.com</td>
                                <td>Adit</td>
                                <td>SI</td>
                                <td>2000</td>
                                <td>5</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-primary">Jawab</a>
                                </td>
                              </tr>
                              <tr>
                                <th scope="row">3</th>
                                <td>01/05/2020</td>
                                <td>Kurikulum</td>
                                <td>SMK</td>
                                <td>Detail</td>
                                <td>Kangadit@gmail.com</td>
                                <td>Adit</td>
                                <td>SI</td>
                                <td>2000</td>
                                <td>5</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-primary">Jawab</a>
                                </td>
                              </tr>
                              <tr>
                                <th scope="row">4</th>
                                <td>01/05/2020</td>
                                <td>Kurikulum</td>
                                <td>SMK</td>
                                <td>Detail</td>
                                <td>Kangadit@gmail.com</td>
                                <td>Adit</td>
                                <td>SI</td>
                                <td>2000</td>
                                <td>5</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-primary">Jawab</a>
                                </td>
                              </tr>
                              <tr>
                                <th scope="row">5</th>
                                <td>01/05/2020</td>
                                <td>Kurikulum</td>
                                <td>SMK</td>
                                <td>Detail</td>
                                <td>Kangadit@gmail.com</td>
                                <td>Adit</td>
                                <td>SI</td>
                                <td>2000</td>
                                <td>5</td>
                                <td>
                                    <a href="#" class="btn btn-sm btn-primary">Jawab</a>
                                </td>
                              </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>
        </div>
    </section>
@endsection           