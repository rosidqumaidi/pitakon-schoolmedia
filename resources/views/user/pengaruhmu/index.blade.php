@extends('user._layouts.main')

@section('pengaruhmuActive')
    {{ 'active' }}
@endsection

@section('content-user')
   
@endsection

<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">No</th>
        <th scope="col">Waktu Buat</th>
        <th scope="col">Username</th>
        <th scope="col">ID Pertanyaan</th>
        <th scope="col">ID Jawaban</th>
        <th scope="col">JP</th>
        <th scope="col">Rating</th>
        <th scope="col">Respon</th>
        <th scope="col">Terpilih</th>
        <th scope="col">Poin</th>
        <th scope="col">Scoin</th>
    </tr>
    </thead>
    <tbody>
        <?php $i = 0 ?>
        @foreach($pitakon as $item)
        <?php $i++ ?>
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $item->created_at }}</td>
            @foreach($item->user as $data)
                <td>{{ $data['username'] }}</td>
            @endforeach

            <td>{{ $item->id }}</td>

            @foreach($item->answer as $data)
                <td>{{ $data->id }}</td>
            @endforeach
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>
                <div class="form-check form-switch">
                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                </div>
            </td>
            <td>-</td>
            <td>-</td>
        </tr>
        @endforeach
    </tbody>
</table>
