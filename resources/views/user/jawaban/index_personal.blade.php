@extends('user._layouts.main')

@section('jawabanActive')
    {{ 'active' }}
@endsection

@section('content-user')
<section class="content">
    <div class="row">
        <main class=" px-md-4" style=" padding-right: 0px!important; padding-left: 10px!important;">
            <div class="bg-white flex-wrap flex-md-nowrap align-items-center pt-3 pb-4 pl-4 pr-4 mb-3 border-bottom">
                <div class="col-sm-12 d-flex justify-content-center mt-4">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Waktu Buat</th>
                            <th scope="col">Kategori</th>
                            <th scope="col">Jenjang</th>
                            <th scope="col">ID Pertanyaan</th>
                            <th scope="col">Pertanyaan</th>
                            <th scope="col">Username</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Scoin</th>
                            <th scope="col">Scoin UP</th>
                            <th scope="col">Approve</th>
                            <th scope="col">Poin</th>
                            <th scope="col">Scoin</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0 ?>
                            @foreach($pitakon as $item)
                            <?php $i++ ?>
                            <tr>
                                <td>{{ $i }}</td>
                                <td>{{$item->created_at}}</td>

                                @foreach($item->category as $data)
                                    <td>{{ $data->category }}</td>
                                    <td>{{ $data->ladder }}</td>
                                @endforeach

                                <td>{{ $item->id }}</td>
                                <td>{{$item->essay}}</td>

                                @foreach($item->user as $data)
                                    <td>{{ $data->username }}</td>
                                    <td>{{ $data->name }}</td>
                                @endforeach

                                <td>-</td>
                                <td>-</td>
                                <td>
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" checked>
                                    </div>
                                </td>
                                <td>-</td>
                                <td>-</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </main>
    </div>
</section>
@endsection