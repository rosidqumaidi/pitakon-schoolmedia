<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = "answer";

    // Relasi tabel answer dengan tabel question 
    public function question()
    {
        return $this->belongsTo('App\Model\Question');
    }
}
