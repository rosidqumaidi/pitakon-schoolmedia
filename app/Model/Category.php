<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";

    // Relasi tabel Category dengan Tabel Question
    public function question()
    {
        return $this->belongsTo('App\Model\Question');
    }
}
