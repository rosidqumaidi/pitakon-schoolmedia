<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InquiriesFromExchanges extends Model
{
    protected $table = "inquiries_from_exchanges";
}
