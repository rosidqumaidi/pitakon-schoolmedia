<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "question";

    // Relasi tabel user dengan tabel question
    public function user()
    {
        return $this->hasMany('App\Model\User');
    }

    // Relasi tabel category dengan tabel question
    public function category()
    {
        return $this->hasMany('App\Model\Category');
    }

    // Relasi tabel question dengan tabel answer
    public function answer()
    {
        return $this->hasMany('App\Model\Answer');
    }
} 
