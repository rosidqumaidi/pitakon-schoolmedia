<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class InquiriesToExchanges extends Model
{
    protected $table = "inquiries_to_exchanges";
}
