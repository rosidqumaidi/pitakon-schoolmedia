<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ThemeCategory extends Model
{
    protected $table = "theme_category";
}
