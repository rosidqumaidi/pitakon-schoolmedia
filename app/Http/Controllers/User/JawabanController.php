<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Question;

class JawabanController extends Controller
{
    // Jawaban Dari Personal
    public function index_personal()
    {
        $data['pitakon'] = Question::all();

        return view('user.jawaban.index_personal', $data);
    }

    // Jawaban Dari Bursa
    public function index_bursa()
    {
        $data['pitakon'] = Question::all();

        return view('user.jawaban.index_bursa', $data);
    }

    // Detail Jawaban
    public function detail_jawaban()
    {
        $data['pitakon'] = Question::all();

        return view('user.jawaban.detail_jawaban', $data);
    }
}
