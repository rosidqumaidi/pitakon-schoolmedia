<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Question;

class PitakonController extends Controller
{
    // Partisipasi
    public function partisipasi_kurikulum()
    {
        $data['pitakon'] = Question::all();

        return view('user.partisipasi.index_kurikulum', $data);
    }

    public function partisipasi_tema()
    {
        return view('user.partisipasi.index_tema');
    }

    // Histori
    public function histori()
    {
        return view('user.histori.index');
    }
}