<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home


// User
Route::get('home', 'HomeController@index')->name('home');

// Pengaruhmu
Route::get('pitakon/pengaruhmu', 'User\PengaruhmuController@index')->name('user.pengaruhmu.index');

// Pertanyaan
Route::get('pitakon/pertanyaan', 'User\PertanyaanController@index')->name('user.pertanyaan.index');

// Jawaban
Route::get('pitakon/jawaban-personal', 'User\JawabanController@index_personal')->name('user.jawaban.index_personal');
Route::get('pitakon/jawaban-bursa', 'User\JawabanController@index_bursa')->name('user.jawaban.index_bursa');
Route::get('pitakon/detail-jawaban', 'User\JawabanController@detail_jawaban')->name('user.jawaban.detail_jawaban');

// Partisipasi
Route::get('pitakon/partisipasi-kurikulum', 'User\PitakonController@partisipasi_kurikulum')->name('user.partisipasi.index_kurikulum');
Route::get('pitakon/partisipasi-tema', 'User\PitakonController@partisipasi_tema')->name('user.partisipasi.index_tema');

// Histori
Route::get('pitakon/histori', 'User\PitakonController@histori')->name('user.histori.index');

// Home
Route::get('pitakon/home', 'User\HomeController@index')->name('user.pitakon.home');

// Create Question
Route::get('pitakon/buat', 'User\PertanyaanController@create')->name('user.pitakon.buat');
Route::post('store', 'User\PertanyaanController@store')->name('user.pitakon.store');

// Route::get('pitakona2', 'User\PitakonController@pitakona2')->name('user.pitakona2');
// Route::get('pitakonb2', 'User\PitakonController@pitakonb2')->name('user.pitakonb2');


// Admin
Route::get('dashboard', 'Admin\HomeController@index')->name('admin.home.index');

// Pertanyaan
Route::get('dashboard/pertanyaan_personal', 'Admin\PertanyaanController@pertanyaan_personal')->name('admin.pertanyaan.personal');
Route::get('dashboard/pertanyaan_bursa', 'Admin\PertanyaanController@pertanyaan_bursa')->name('admin.pertanyaan.bursa');

// Jawaban
Route::get('dashboard/jawaban_personal', 'Admin\JawabanController@jawaban_personal')->name('admin.jawaban.personal');
Route::get('dashboard/jawaban_bursa', 'Admin\JawabanController@jawaban_bursa')->name('admin.jawaban.bursa');
Route::get('dashboard/detail_jawaban', 'Admin\JawabanController@detail_jawaban')->name('admin.jawaban.detail_jawaban');

// Partisipasi
Route::get('dashboard/partisipasi', 'Admin\PitakonController@partisipasi')->name('admin.partisipasi.index');

// Histori
Route::get('dashboard/histori', 'Admin\PitakonController@histori')->name('admin.histori.index');
